FROM golang:1.12.5-alpine3.9 AS build
ARG HUGO_VERSION=0.62.0
ENTRYPOINT [ "hugo" ]
CMD [ "-h" ]
EXPOSE 1313
WORKDIR /wd
RUN apk update && \
    apk upgrade && \
    apk add --no-cache libc6-compat libstdc++
RUN wget https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz -O hugo.tar.gz && \
    tar -xzvf hugo.tar.gz && \
    rm -rf hugo.tar.gz && \
    mv hugo /usr/local/bin
COPY . /wd
RUN hugo

FROM nginxinc/nginx-unprivileged:1-alpine
COPY --from=build --chown=101 /wd/public /usr/share/nginx/html
CMD ["nginx","-g","daemon off;"]
