#!/bin/bash
# Strict Mode
set -euo pipefail
IFS=$'\n\t'

# Link and set hooks to executable
root="$(pwd)"
sudo ln -sf "$root/hooks" "$root/.git/"
