output "bucket_website_endpoint" {
  value = aws_s3_bucket.site.website_endpoint
}
output "bucket_clickable_link" {
  value = "http://${aws_s3_bucket.site.website_endpoint}"
}
output "cdn_domain_name" {
  value = aws_cloudfront_distribution.distribution.domain_name
}
output "cdn_hosted_zone_id" {
  value = aws_cloudfront_distribution.distribution.hosted_zone_id
}
